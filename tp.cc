
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <queue>
#include <map>
#include <stack>
#include <list>
#include <limits>
#include <tuple>

using namespace std;

#define MAX_VERTICE		 		500
#define NULO					-1

typedef int Peso;

struct Aresta {
    int v1;
    int v2;
    int distance;
};

bool sortArestaFunction(const Aresta aresta1,const Aresta aresta2) {
    return aresta1.distance < aresta2.distance;
}

class Vertice
{
public:
    int vertice;
    int peso;

    Vertice (int v){
        this->vertice = v;
    }

    Vertice (int v,int p){
        vertice = v;
        peso = p;
    }
    
    bool operator==(const Vertice &v){
		return this->vertice == v.vertice;
	}
};

//=====================================================================
// CLASSE GRAFO
//=====================================================================
class Grafo
{
private:
    int numVertice,
    numAresta;
    Peso matriz[MAX_VERTICE][MAX_VERTICE];
    vector<list<Vertice*> > vertices;

public:
    //--------------------------------------------------------------------
    // Construtor
    //--------------------------------------------------------------------
    Grafo()
    {
        numVertice = 0;
        excluirTodasArestas();
    }//-------------------------------------------------------------------


    //--------------------------------------------------------------------
    // Destrutor
    //--------------------------------------------------------------------
    ~Grafo()
    {
    }//-------------------------------------------------------------------


    //--------------------------------------------------------------------
    // lerGrafo: Realiza a leitura do grafo no arquivo.
    //--------------------------------------------------------------------
    bool lerGrafo()
    {
        bool resp;
        int temp;        

        //Ler o numero de vertices
        cin >> temp;
        setNumVertice(temp);

        resp = (numVertice > 0) ? true : false;
        
        for(int i = 0; i < numVertice; i++)
        {
            inserirAresta(i, i, NULO);
            for(int j = i+1; j < numVertice; j++)
            {
                cin >> temp;
                inserirAresta(i, j, temp);
                inserirAresta(j, i, temp);
            }
        }
        return resp;
    }//-------------------------------------------------------------------
    
    void imprimir (){
		for (int i=0;i<this->numVertice;i++){
			cout<<"["<<i<<"] = ";
			for (list<Vertice*>::iterator it= vertices[i].begin(); it != vertices[i].end();++it){
				cout<<"["<<(*it)->vertice<<"] ";
			}
			cout<<"\n";
		}
		cout<<"\n";
	}
	
	//--------------------------------------------------------------------
    // isCompleto: Verifica se o grafo é completo ou não
    // -------------------------------------------------------------------
	bool isCompleto (){
		return numAresta == (numVertice * (numVertice - 1)) / 2;
	}

    //--------------------------------------------------------------------
    // getNumVertice: retorna o numero de vestices do grafos
    // -------------------------------------------------------------------
    int getNumVertice()
    {
        return this->numVertice;
    }//--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // getNumAresta: retorna o numero de arestas do grafo
    // -------------------------------------------------------------------
    int getNumAresta()
    {
        return this->numAresta;
    }//-------------------------------------------------------------------
    
    void imprimirBuscaEmLargura (){
        buscaEmLargura();
    }
    
    void imprimirBuscaEmProfundidade (){
        buscaEmProfundidade();
    }
    
    //--------------------------------------------------------------------
    // isCiclico: Verifica se o grafo contem ciclos
    //--------------------------------------------------------------------
    bool isCiclico (){
        bool * visitados = new bool [this->numVertice];
        for (int i=0; i < this-> numVertice; i++) {
            visitados[i] = false;
        }
        for (int i=0;i < this->numVertice;i++) {
            if (!visitados[i]) {
                if (!isCiclico (i,visitados)){
                    return false;
                }
            }
        }
        cout<<"\n";
        delete [] visitados;
        return true;
    }//--------------------------------------------------------------------
    
    void imprimirIsHamiltoniano() {
        bool * visitados = new bool [this->numVertice];
        int * caminho = new int [this->numVertice +1];
        for (int i=0; i < this-> numVertice; i++) {
            visitados[i] = false;
            caminho[i] = -1;
        }

        int dist = hamilton (0,visitados,caminho,0 , 0);
        
        if (dist < 0) {
            cout<<"O grafo não é halmitoniano\n";
        } else {
            cout<<"O grafo é halmitoniano um dos seus ciclos é:\n";
            for (int i=0;i<this->numVertice +1;i++){
                cout<<caminho[i]<<" ";
            }
        }
        
        cout<<"\n";
        delete [] visitados;
        delete [] caminho;
    }
    
    //--------------------------------------------------------------------
    // caxeiroViajante: Encontra a solucao otima para o caxeiro viajante
    //--------------------------------------------------------------------
    void imprimirCaxeiroViajante (){
        
        if (!this->isCompleto()) {
            cout<<"Error, o grafo informado não é completo.";
            return;
        }
        
        bool * visitados = new bool [this->numVertice];
        int * melhorCaminho = new int [this->numVertice +1];
        int * caminho = new int [this->numVertice +1];
        int distanciaTotal = -1;
        for (int i=0; i < this-> numVertice; i++) {
            visitados[i] = false;
            caminho[i] = -1;
            melhorCaminho[i] = -1;
        }
        caxeiroViajante (0,visitados,caminho,melhorCaminho,distanciaTotal,0);
        for (int i=0;i<this->numVertice +1;i++){
            cout<<melhorCaminho[i]<<" ";
        }
        cout<<"\n";
        delete [] visitados;
        delete [] melhorCaminho;
        delete [] caminho;
    }//--------------------------------------------------------------------
    
    //--------------------------------------------------------------------
    // caxeiroViajante: Encontra a solucao otima para o caxeiro viajante
    //--------------------------------------------------------------------
    void imprimirCaxeiroViajanteBB (){
        
        if (!this->isCompleto()) {
            cout<<"Error, o grafo informado não é completo.";
            return;
        }
        
        bool * visitados = new bool [this->numVertice];
        int * melhorCaminho = new int [this->numVertice +1];
        int * caminho = new int [this->numVertice +1];
        int distanciaTotal = -1;
        for (int i=0; i < this-> numVertice; i++) {
            visitados[i] = false;
            caminho[i] = -1;
            melhorCaminho[i] = -1;
        }
        caxeiroViajanteBB (0,visitados,caminho,melhorCaminho,distanciaTotal,0);
        for (int i=0;i<this->numVertice +1;i++){
            cout<<melhorCaminho[i]<<" ";
        }
        cout<<"\n";
        delete [] visitados;
        delete [] melhorCaminho;
        delete [] caminho;
    }//--------------------------------------------------------------------


	
	//--------------------------------------------------------------------
	// getGrafoComplementar: retorna o grafo Complementar
	//--------------------------------------------------------------------
	Grafo * getGrafoComplementar() {
		Grafo * g = new Grafo();
        g->setNumVertice(this->numVertice);
		for (int i = 0; i < this->numVertice; i++){
			for (int j= i+ 1;j<this->numVertice ;j++){
				if (!isAresta(i,j)){
					g->inserirAresta(i, j, 1);
					g->inserirAresta(j, i, 1);
				} 
			}
		}
		return g;
	}//-------------------------------------------------------------------
    
    
    
    //--------------------------------------------------------------------
    // getNumComponentes: Retorna o número de componentes no grafo
    //--------------------------------------------------------------------
    int getNumComponentes() {
        //cada indice corresponde a um vertice
        int * componentes = new int [this->numVertice];
        int numComponentes = 0;
        for (int i=0;i<this->numVertice;i++){
            // -1 , diz que o vertice ainda não tem componente definida
            componentes[i] = -1;
        }
        for (int i=0;i<this->numVertice;i++){
            //se o vertice não tem componente ainda, definir as mesmas
            if (componentes[i] == -1){
                definirComponentes(i,numComponentes++,componentes);
            }
        }
        delete [] componentes;
        return numComponentes;
    }//-------------------------------------------------------------------
    
    Grafo* kruskal() {
        Grafo *agm = new Grafo();
        agm->setNumVertice(this->numVertice);
        list<Aresta> arestas = list<Aresta>();
        list<Vertice *>::iterator it;
        for (int i =0; i < this->numVertice; i++) {
            for (it=vertices[i].begin(); it!=vertices[i].end(); ++it) {
                Aresta a = Aresta();
                a.v1 = i;
                a.v2 = (*it)->vertice;
                a.distance = getAresta(a.v1, a.v2);
                arestas.push_back(a);
            }
        }
        
        arestas.sort(sortArestaFunction);
        
        list<Aresta>::iterator arestasIt;
        for (arestasIt=arestas.begin(); arestasIt!=arestas.end(); ++arestasIt) {
            Aresta a = (*arestasIt);
            agm->inserirAresta(a.v1, a.v2, a.distance);
            
            if (agm->isCiclico()) {
                agm->removerAresta(a.v1, a.v2);
            }
        }
        return agm;
    }


private:

    void removerVertice (int v){
		Vertice * vizinho = vertices[v].front();				
		list<Vertice *>::iterator it;
		for (it=vertices[vizinho->vertice].begin(); it!=vertices[vizinho->vertice].end(); ++it){
			if((*it)->vertice == v){
		        it  = vertices[vizinho->vertice].erase(it);
			}
		}
		vertices[v].clear();
		delete vizinho;
		this->numVertice--;
	}

    //--------------------------------------------------------------------
    // definirComponentes: define as componentes em profundidade
    // recursivamente
    //--------------------------------------------------------------------
    void definirComponentes (int v,int componente,int * componentes){
		  componentes[v] = componente;
		  list<Vertice *>::iterator it;
		  list<Vertice *>vizinhos = vertices[v];
		  for (it=vizinhos.begin() ; it != vizinhos.end(); it++ ){
			  if (componentes[(*it)->vertice] == -1){
				definirComponentes((*it)->vertice,componente,componentes);
			  }
		  }
    }//-------------------------------------------------------------------

	bool isConexo (){		
		return this->getNumComponentes() == 1;
	}

	//--------------------------------------------------------------------
	// isArvore: Verifica se o grafo é uma ávore
	//--------------------------------------------------------------------
	bool isArvore (){		
		return isConexo() && ((this->numAresta + 1) == this->numVertice);
	}//-------------------------------------------------------------------	

    //--------------------------------------------------------------------
    // inserirAresta: Insere uma nova aresta.
    //--------------------------------------------------------------------
    void inserirAresta(int v1, int v2, Peso peso)
    {
        if (peso == NULO)
            return;
        if(v1 > MAX_VERTICE)
        {
            printf("ERRO! Vertice %i nao existe no grafico", v1);
            return;
        }
        if(v2 > MAX_VERTICE)
        {
            printf("ERRO! Vertice %i nao existe no grafico", v2);
            return;
        }
        vertices[v1].push_back (new Vertice(v2,peso));
        if(!isAresta(v2,v1))
        {
          numAresta++;
        }

    }//-------------------------------------------------------------------
    
    void removerAresta(int v1, int v2) {
        list <Vertice*>::iterator it=vertices[v1].begin();
        while (it != vertices[v1].end()) {
            if ((*it)->vertice == v2) {
                it = vertices[v1].erase(it);
            } else {
                ++it;
            }
        }
        
        it = vertices[v2].begin();
        while (it != vertices[v2].end()) {
            if ((*it)->vertice == v1) {
                it = vertices[v2].erase(it);
            } else {
                ++it;
            }
        }
        numAresta--;
    }


    //--------------------------------------------------------------------
    // isAresta: Retorna true se existe a aresta.
    //--------------------------------------------------------------------
    bool isAresta(int v1, int v2)
    {
        list <Vertice*> vizinhos = vertices[v1];
        list <Vertice*>::iterator it;
        for (it=vizinhos.begin(); it!=vizinhos.end(); ++it)
        {
            if ((*it)->vertice == v2)
            {
                return true;
            }
        }
        return false;
    }//-------------------------------------------------------------------

    //--------------------------------------------------------------------
    // getAresta: Retorna o peso da aresta.
    //--------------------------------------------------------------------
    Peso getAresta(int v1, int v2)
    {
        list <Vertice*> vizinhos = vertices[v1];
        list <Vertice*>::iterator it;
        for (it=vizinhos.begin(); it!=vizinhos.end(); ++it)
        {
            if ((*it)->vertice == v2)
            {
                return (*it)->peso;
            }
        }
        return -1;
    }//-------------------------------------------------------------------

    //--------------------------------------------------------------------
    // excluirAresta: Exclui uma aresta.
    //--------------------------------------------------------------------
    void excluirAresta(int v1, int v2)
    {
        if(v1 > numVertice)
        {
            printf("ERRO! Vertice %i nao existe no grafico",v1);
            return;
        }

        if(v2 > numVertice)
        {
            printf("ERRO! Vertice %i nao existe no grafico",v2);
            return;
        }
        vertices[v1].remove(new Vertice (v1));
        vertices[v2].remove(new Vertice (v2));

    }//-------------------------------------------------------------------

    //--------------------------------------------------------------------
    // excluirTodasArestas: Exclui todas as arestas.
    //--------------------------------------------------------------------
    void excluirTodasArestas()
    {
        for(int i = 0; i <numVertice; i++)
        {
            vertices[i].clear();
        }
        numAresta = 0;
    }//-------------------------------------------------------------------

    //--------------------------------------------------------------------
    // setNumVertice: Altera a variavel numVertice.
    //--------------------------------------------------------------------
    void setNumVertice(int nVertice)
    {

        if(nVertice > MAX_VERTICE)
        {
            printf("ERRO: Numero de vertices\n");
            return;
        }
        numVertice = nVertice;
        for(int i = 0; i < numVertice; i++)
        {
            vertices.push_back (list<Vertice*>());
        }
    }//-------------------------------------------------------------------
    
    //---------------------------------------------------------------------
    // buscaEmLargura: percorre o grafo fazendo buca em largura
    //---------------------------------------------------------------------
    void buscaEmLargura (){
        bool * visitados = new bool [this->numVertice];
        for (int i=0; i < this-> numVertice; i++) {
            visitados[i] = false;
        }
        buscaEmLargura (0,visitados);
        cout<<"\n";
        delete [] visitados;
    }
    
    //--------------------------------------------------------------------
    // getGrau: Retorna o grau do Vertice
    //--------------------------------------------------------------------
    int getGrau (Vertice v){
        int grau = 0;
        for (int i=0;i<this->numVertice;i++){
            if (isAresta(v.vertice,i) || isAresta(i,v.vertice)){
                grau++;
            }
        }
        return grau;
    }//-------------------------------------------------------------------
    
    //---------------------------------------------------------------------
    // getVizinhos: retorna os vertices adjacentes
    //---------------------------------------------------------------------
    int * getVizinhos(Vertice v){
        int grau = getGrau(v);
        int * vizinhos = new int [grau];
        int indice =0;
        for (int i=0;i<this->numVertice;i++){
            if (isAresta(v.vertice,i) || isAresta(i,v.vertice)){
                vizinhos[indice] = i;
                indice++;
            }
        }
        return vizinhos;
    }
    
    //---------------------------------------------------------------------
    // buscaEmLargura: percorre a componente realizando a busca em largura
    //---------------------------------------------------------------------
    void buscaEmLargura (Vertice v, bool * visitados){
        visitados[v.vertice] = true;
        queue <Vertice> fila;
        fila.push(v);
        while (!fila.empty()){
            v = fila.front();
            fila.pop();
            cout<<v.vertice<<" ";
            int grau = getGrau(v);
            int * vizinhos = getVizinhos (v);
            
            for (int i= 0; i < grau; i++){
                if (!visitados[vizinhos[i]]){
                    fila.push(vizinhos[i]);	
                }
                visitados[vizinhos[i]] = true;
            }
            delete [] vizinhos;   
        }
    }
    
    //---------------------------------------------------------------------
    // isCiclico: Verifica se o grafo contem ciclos
    //---------------------------------------------------------------------
    bool isCiclico (Vertice v,bool * visitados){
        if (visitados[v.vertice]) {
            return true;
        }
        visitados[v.vertice] = true;
        int grau = getGrau (v);
        int * vizinhos = getVizinhos (v);
        for (int i = 0; i < grau; i ++) {
            if (isCiclico(vizinhos[i],visitados) && v.peso != -1){
                return true;
            }
        }
        delete [] vizinhos;
        
        return false;
    }//-------------------------------------------------------------------
    
    
    
    //--------------------------------------------------------------------
    // buscaEmProfundidade: Percorre o grafo fazendo busca em profundidade
    //--------------------------------------------------------------------
    void buscaEmProfundidade (){
        bool * visitados = new bool [this->numVertice];
        for (int i=0; i < this-> numVertice; i++){
            visitados[i] = false;
        }
        for (int i=0;i < this->numVertice;i++){
            if (!visitados[i]){
                buscaEmProfundidade (i,visitados);
            }
        }
        cout<<"\n";
        delete [] visitados;
    }//--------------------------------------------------------------------
    
    //---------------------------------------------------------------------
    // buscaEmProfundidade : percorre um componente realizando busca em
    // profundidade
    //---------------------------------------------------------------------
    void buscaEmProfundidade (Vertice v,bool * visitados){
        cout<<v.vertice<<" ";
        visitados[v.vertice] = true;
        int grau = getGrau (v);
        int * vizinhos = getVizinhos (v);
        for (int i = 0; i < grau; i ++){
            if (!visitados[vizinhos[i]]){
                buscaEmProfundidade(vizinhos[i],visitados);
            }
        }
        delete [] vizinhos;
    }//-------------------------------------------------------------------
    
    //---------------------------------------------------------------------
    // caxeiroViajante : encontra a solucao Otima para o caxeiro viajante
    //---------------------------------------------------------------------
    int caxeiroViajante (int v,bool * visitados,int * caminho,int * melhorCaminho,int menorDistancia,int indice){
        visitados[v] = true;
        caminho[indice] = v;
        int grau = getGrau (v);
        int * vizinhos = getVizinhos (v);
        indice++;
        for (int i = 0; i < grau; i ++){
            if (!visitados[vizinhos[i]]){
                menorDistancia = caxeiroViajante(vizinhos[i],visitados,caminho,melhorCaminho,menorDistancia,indice);
                visitados[vizinhos[i]] = false;
            }
        }
        if (indice == numVertice) {
            caminho[this->numVertice] = 0;
            int dist = calcularDistanciaTotal (caminho);
            if (dist != -1){
                if (dist < menorDistancia || menorDistancia == -1){
                    menorDistancia = dist;
                    for (int i=0;i<this->numVertice+1;i++){
                        melhorCaminho[i] = caminho[i];
                    }
                }
            }
        }
        delete [] vizinhos;
        return menorDistancia;
    }//-------------------------------------------------------------------
    
    int caxeiroViajanteBB (int v,bool * visitados,int * caminho,int * melhorCaminho,int menorDistancia,int indice){
        visitados[v] = true;
        caminho[indice] = v;
        int grau = getGrau (v);
        int * vizinhos = getVizinhos (v);
        indice++;
        for (int i = 0; i < grau; i ++){
            if (!visitados[vizinhos[i]]) {
                int currentBestDistance = calcularDistanciaTotal(melhorCaminho);
                if (currentBestDistance > 0) {
                    int distanciaParcial = calcularDistanciaTotalBB(caminho, indice);
                    // se a distância parcial já for maior que a distância máxima. Não executar o resto
                    if (distanciaParcial < currentBestDistance) {
                        menorDistancia = caxeiroViajanteBB(vizinhos[i],visitados,caminho,melhorCaminho,menorDistancia,indice);
                        visitados[vizinhos[i]] = false;
                    }
                } else {
                    menorDistancia = caxeiroViajanteBB(vizinhos[i],visitados,caminho,melhorCaminho,menorDistancia,indice);
                    visitados[vizinhos[i]] = false;
                }
            }
        }
        if (indice == numVertice) {
            caminho[this->numVertice] = 0;
            int dist = calcularDistanciaTotal (caminho);
            if (dist != -1){
                if (dist < menorDistancia || menorDistancia == -1){
                    menorDistancia = dist;
                    for (int i=0;i<this->numVertice+1;i++){
                        melhorCaminho[i] = caminho[i];
                    }
                }
            }
        }
        delete [] vizinhos;
        return menorDistancia;
    }//-------------------------------------------------------------------
    
    
    int calcularDistanciaTotalBB (int * caminho, int count) {
        int distancia = 0;
        for (int i=0;i<count;i++) {
            distancia += getAresta(caminho[i],caminho[i+1]);
        }
        return distancia;
    }

    
    //---------------------------------------------------------------------
    // hamilton : encontra um ciclo de hamilton
    //---------------------------------------------------------------------
    int hamilton (int v, bool * visitados, int * caminho, int indice, int dist) {
        
        
        dist = calcularDistanciaTotal(caminho);
        if (dist > 0) {
            return dist;
        }
        
        visitados[v] = true;
        caminho[indice] = v;
        int grau = getGrau (v);
        int * vizinhos = getVizinhos (v);
        indice++;
        
        if (indice == numVertice) {
            caminho[this->numVertice] = caminho[0];
        }                

        for (int i = 0; i < grau; i ++){
            if (!visitados[vizinhos[i]]){
                dist = hamilton(vizinhos[i],visitados,caminho, indice, dist);
                visitados[vizinhos[i]] = false;
            }
        }
        
        delete [] vizinhos;
        return dist;
    }//-------------------------------------------------------------------
    
    
    int calcularDistanciaTotal (int * caminho) {
        for (int i=0;i<this->numVertice;i++) {
            if (caminho[i] == -1) {
                return -1;
            }
        }
        
        if (!isAresta(caminho[this->numVertice-1],caminho[this->numVertice])){
            return -1; //n„o existe volta para o vertice inicial
        }
        
        int distancia = 0;
        for (int i=0;i<this->numVertice;i++) {
            distancia += getAresta(caminho[i],caminho[i+1]);
        }
        return distancia;
    }
    
};


//=====================================================================
// FUNCAO PRINCIPAL
//=====================================================================
int main(int argc, char **argv)
{

    Grafo *g = new Grafo;
    vector<Grafo *> grafos;
    while (g->lerGrafo() == true)
    {
        grafos.push_back(g);
        g = new Grafo;
    }
    int n = grafos.size();
    for (int i=0; i<n; i++)
    {
        g = grafos[i];
        cout<<"Analisando o grafo: \n\n";
        g->imprimir();
        if (g->isCompleto()) {
            cout<<"1 - O grafo é completo.\n";
        } else {
            cout<<"1 - O grafo não é completo.\n";
        }
        
        cout<<"2 - Seu grafo complementar é:\n";
        
        Grafo *complementar = g->getGrafoComplementar();
        complementar->imprimir();
        
        cout<<"3 - Busca em largura:\n";
        g->imprimirBuscaEmLargura();
        
        cout<<"4 - Busca em profundidade:\n";
        g->imprimirBuscaEmProfundidade();
        
        cout<<"5 - Número de componentes: "<<g->getNumComponentes()<<"\n";
        
        cout<<"6 - Grafo Hamiltoniano\n";
        g->imprimirIsHamiltoniano();
        
        Grafo *agm = g->kruskal();
        cout<<"Sua AGM é\n";
        agm->imprimir();
        g->imprimirCaxeiroViajante();
        g->imprimirCaxeiroViajanteBB();
        
        cout<<"-------------------\n\n";
                
        
    }
    delete g;

    return 0;
}//--------------------------------------------------------------------

