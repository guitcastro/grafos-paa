CC=clang++ 
COMPILE_FLAGS=-lc++ -g

.PHONY:tp

tp:
	${CC} ${COMPILE_FLAGS}  tp.cc -o tp
